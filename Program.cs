﻿using System;
using System.Text.RegularExpressions;

namespace KataPasswordStrengthChecker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            Console.WriteLine(PasswordChecker("password"));
        }
        public static string PasswordChecker(string password)
        {
            string englishCharsUpper = "[A-Zi]";
            string englishCharsLower = "[a-zi]";
            Regex rxEnglishCharsUpper = new Regex(englishCharsUpper);
            Regex rxEnglishCharsLower = new Regex(englishCharsLower);
            string digits = "[0-9i]";
            Regex rx1Digits = new Regex(digits);
            string specialCharacters = "[!\"#$%&'()*'+',-./:;<=>?@^_`{|}~i]";
            Regex rx2SpecialCharacters = new Regex(specialCharacters);

            int matchesCounter = 0;

            if (rxEnglishCharsUpper.IsMatch(password)) matchesCounter++;
            if (rxEnglishCharsLower.IsMatch(password)) matchesCounter++;
            if (rx1Digits.IsMatch(password)) matchesCounter++;
            if (rx2SpecialCharacters.IsMatch(password)) matchesCounter++;
            if (password.Length > 7) matchesCounter++; else matchesCounter = 0;
            if (password.Contains(" ")) matchesCounter = 0;


            switch (matchesCounter)
            {
                case 1:
                    return "Weak";
                case 2:
                    return "Weak";
                case 3:
                    return "Moderate";
                case 4:
                    return "Moderate";
                case 5:
                    return "Strong";
                default:
                    return "Invalid";
            }

        }
    }
}
